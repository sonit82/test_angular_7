﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MimeKit;
using TestAngular7.Helpers;
using TestAngular7.Models;

namespace TestAngular7.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly ILogger<EmailController> _logger;
        readonly IEmailSender _emailer;

        public EmailController(ILogger<EmailController> logger, IEmailSender emailer)
        {
            _logger = logger;
            _emailer = emailer;
        }

        [AllowAnonymous]
        [HttpPost("NewEmail")]
        public async Task<string> NewEmail([FromBody] EmailModel email)
        {
            MailboxAddress[] to = null;
            MailboxAddress[] cc = null;
            MailboxAddress[] bcc = null;
            
            string[] emailTo=null;
            if (!string.IsNullOrEmpty(email.To))
            {
                emailTo = email.To.Split(";", StringSplitOptions.RemoveEmptyEntries);
                to = new MailboxAddress[emailTo.Length];
                for (int i = 0; i < emailTo.Length; i++)
                {
                    if (IsValidEmail(emailTo[i]))
                        to[i] = new MailboxAddress(emailTo[i]);
                    else
                        return string.Format("Invalid To email address:{0}", emailTo[i]);
                }
            }

            string[] emailCc = null;
            if (!string.IsNullOrEmpty(email.Cc))
            {
                emailCc = email.Cc.Split(";", StringSplitOptions.RemoveEmptyEntries);
                cc = new MailboxAddress[emailCc.Length];
                for (int i = 0; i < emailCc.Length; i++)
                {
                    if (IsValidEmail(emailCc[i]))
                        cc[i] = new MailboxAddress(emailCc[i]);
                    else
                        return string.Format("Invalid CC email address:{0}", emailCc[i]);

                }
            }

            string[] emailBcc = null;
            if (!string.IsNullOrEmpty(email.Bcc))
            {
                emailBcc = email.Bcc.Split(";", StringSplitOptions.RemoveEmptyEntries);
                bcc = new MailboxAddress[emailBcc.Length];
                for (int i = 0; i < emailBcc.Length; i++)
                {
                    if (IsValidEmail(emailBcc[i]))
                        bcc[i] = new MailboxAddress(emailBcc[i]);
                    else
                        return string.Format("Invalid BCC email address:{0}", emailBcc[i]);
                }
            }
            (bool success, string errorMsg) = await _emailer.SendEmailAsync(new MailboxAddress(email.From), to, cc, bcc, email.Subject, email.Message);

            if (success)
                return "Email has been sent successfully.";

            return errorMsg;
        }

        private bool IsValidEmail(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
       
}