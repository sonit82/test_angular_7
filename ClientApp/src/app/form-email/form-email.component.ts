import { Component, OnInit } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

import { FormGroup, FormControl } from '@angular/forms';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-form-email',
  templateUrl: './form-email.component.html',
  styleUrls: ['./form-email.component.scss']
})
export class FormEmailComponent implements OnInit {
  public Editor = ClassicEditor;
  mail_to = '';
  mail_cc = '';
  mail_bcc = '';
  from = 'testemailsender020@gmail.com';
  subject = '';
  data = '';
  errors = '';
  success='';

  cbb_mail_bcc = '';
  cbb_mail_cc = '';
  cbb_mail_to = '';

  constructor(private http: HttpClient) { }

  ngOnInit() {

  }
  onKey(event: any) { // without type info

      if(event.target.name == "mail_to"){
          this.mail_to = event.target.value;
      }else if(event.target.name == "mail_cc"){
          this.mail_cc = event.target.value;
      }else if(event.target.name == "mail_bcc"){
          this.mail_bcc = event.target.value;
      }else if(event.target.name == "form"){
          this.from = event.target.value;
      }else if(event.target.name == "subject"){
          this.subject = event.target.value;
      }


  }

  onChange( { editor }: ChangeEvent ) {
      this.data = editor.getData();
  }

  onChangeCbb( cbb_tar ) {
        if(cbb_tar.target.name == "cbb_mail_to"){
            this.cbb_mail_to = cbb_tar.target.value;
        }else if(cbb_tar.target.name == "cbb_mail_cc"){
            this.cbb_mail_cc = cbb_tar.target.value;
        }else if(cbb_tar.target.name == "cbb_mail_bcc"){
            this.cbb_mail_bcc = cbb_tar.target.value;
        }
    }

  expression(){
      if(this.mail_to == ''){
          this.errors = 'Mail To not empty !';
          return;
      }else if(this.subject == ''){
          this.errors = 'Subject not empty !';
          return;
      }else if(this.data == ''){
        this.errors = 'Message not empty !';
        return;
      }
      this.errors = '';
      const headers = {'Content-Type':'application/json'};
      const body = { From: this.from, To: this.mail_to, cc:this.mail_cc, bcc:this.mail_bcc, Subject:this.subject, Message:this.data};
      this.success='Sending email...';
      this.http.post('http://localhost:63804/Email/newemail', body, { headers, responseType: 'text'}).
      subscribe({
        next:data => {
            console.log(data);
            this.success=JSON.stringify(data);
            this.errors='';
        },
        error:error=>{
            console.log(error);
            this.errors=JSON.stringify(error);
            this.success='';
        }
    });
  }
}
